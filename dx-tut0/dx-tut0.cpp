#include "stdafx.h"

#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#define GLM_FORCE_RADIANS
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL

#include <windows.h>

#include <d3d11.h>

#include <GL/glew.h>
#include <GL/wglew.h>
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#include <OVR.h>
#include <Displays/OVR_Win32_ShimFunctions.h>

#include "Primitive.h"
#include "Shader.h"

const char* vs = R"(
void main()
{
    gl_Position = gl_Vertex;
    gl_TexCoord[0] = gl_MultiTexCoord0;
}
)";

const char* fs = R"(
uniform sampler2D tex0;
void main()
{
gl_FragColor = texture2D(tex0, gl_TexCoord[0].st);
}
)";

IDXGISwapChain *swapchain;
ID3D11Device *device;
ID3D11DeviceContext *context;
ID3D11RenderTargetView *backbuffer;

ovrHmd hmd;

GLFWwindow* window;
Plane plane;
Shader shader;

LRESULT CALLBACK MainProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp)
{
    switch (msg)
    {
    case WM_CREATE:
        return true;
    case WM_CLOSE:
        PostQuitMessage(0);
        return true;
    }
    return DefWindowProc(hWnd, msg, wp, lp);
}


int _tmain(int argc, _TCHAR* argv[])
{
    ovr_Initialize();
    ovr_InitializeRenderingShim();
    hmd = ovrHmd_Create(0);

    int width = hmd->Resolution.w;
    int height = hmd->Resolution.h;

    WNDCLASS wc{ 0 };
    wc.hInstance = GetModuleHandle(0);
    wc.lpfnWndProc = MainProc;
    wc.lpszClassName = "MainWindow";
    wc.style = CS_HREDRAW | CS_VREDRAW;
    RegisterClass(&wc);
    HWND hWnd = CreateWindow("MainWindow", "DX-Test0", WS_OVERLAPPEDWINDOW, 
        0, 0, width/4, height/4, NULL, NULL, GetModuleHandle(0), NULL);
    ShowWindow(hWnd, SW_NORMAL);

    // Init D3D
    HRESULT res;
    DXGI_SWAP_CHAIN_DESC scd{ 0 };
    scd.BufferCount = 1;
    scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    scd.BufferDesc.Width = width;
    scd.BufferDesc.Height = height;
    scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    scd.OutputWindow = hWnd;
    scd.SampleDesc.Count = 4;
    scd.Windowed = TRUE;
    res = D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, 
        nullptr, 0, D3D11_SDK_VERSION, &scd, &swapchain, &device, nullptr, &context);
    
    ID3D11Texture2D *dx_tex;
    swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&dx_tex));

    device->CreateRenderTargetView(dx_tex, nullptr, &backbuffer);
    dx_tex->Release();
    context->OMSetRenderTargets(1, &backbuffer, nullptr);

    D3D11_VIEWPORT vp{ 0 };
    vp.Width = (FLOAT)width;
    vp.Height = (FLOAT)height;
    context->RSSetViewports(1, &vp);

    // Init Oculus
    if (hmd)
    {
        if (!ovrHmd_AttachToWindow(hmd, hWnd, nullptr, nullptr))
            printf("Direct Mode failed\n");

        ovrHmd_SetEnabledCaps(hmd, ovrHmdCap_LowPersistence);
        ovrHmd_ConfigureTracking(hmd, ovrTrackingCap_Orientation |
            ovrTrackingCap_MagYawCorrection | ovrTrackingCap_Position, 0);

        OVR::Win32::DisplayShim::GetInstance().Active = true;
    }

    // Init GLFW
    if (!glfwInit())
    {
        printf("GLFW init error\n");
        return -1;
    }
    glfwWindowHint(GLFW_REFRESH_RATE, 75);
    glfwWindowHint(GLFW_AUTO_ICONIFY, 0);
    if (!(window = glfwCreateWindow(width/4, height/4, "GLFW window", nullptr, nullptr)))
    {
        return false;
    }
    glfwMakeContextCurrent(window);
    glewInit();

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    plane.create(1, 1);
    if (!shader.load(vs, fs))
        return -1;

    // Connect D3D and GL
    HANDLE gl_handleD3D = wglDXOpenDeviceNV(device);

    // register the Direct3D color and depth/stencil buffers as
    // 2D multisample textures in opengl
    GLuint gl_tex;
    HANDLE gl_handle;

    glGenTextures(1, &gl_tex);

    gl_handle = wglDXRegisterObjectNV(gl_handleD3D, dx_tex, gl_tex, GL_RENDERBUFFER, WGL_ACCESS_READ_WRITE_NV);

    static unsigned char* data = new unsigned char[width * height * 4];
    for (int i = 0; i < width * height; i++)
    {
        unsigned char* p = data + i * 4;
        p[0] = 0xFF;
        p[1] = 0xAA;
        p[2] = 0x10;
        p[3] = 0xFF;
    }

    GLuint texID;
    glGenTextures(1, &texID);
    glBindTexture(GL_TEXTURE_2D, texID);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

    //gl_handles[1] = wglDXRegisterObjectNV(gl_handleD3D, dxDepthBuffer,
    //    gl_names[1],
    //    GL_TEXTURE_2D_MULTISAMPLE,
    //    WGL_ACCESS_READ_WRITE_NV);
    /*
    GLuint fboID;
    GLuint rboID;

    static char* data = new char[width * height * 4];
    for (int i = 0; i < height * height; i++)
    {
        char* p = data + i * 4;
        p[0] = 0xFF;
        p[1] = 0xAA;
        p[2] = 0x10;
        p[3] = 0xFF;
    }
    glBindTexture(GL_TEXTURE_2D, gl_names[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);


    glGenTextures(1, &texID);
    glBindTexture(GL_TEXTURE_2D, texID);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    // Create a renderbuffer object to store depth info
    glGenRenderbuffers(1, &rboID);
    glBindRenderbuffer(GL_RENDERBUFFER, rboID);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    // Create a framebuffer object
    glGenFramebuffers(1, &fboID);
    glBindFramebuffer(GL_FRAMEBUFFER, fboID);

    // Attach the texture to FBO color attachment point
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
        GL_RENDERBUFFER, gl_names[0], 0);

    // Attach the renderbuffer to depth attachment point
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 
        GL_RENDERBUFFER, rboID);

    // Check FBO status
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
        printf("createColorBuffer failed\n");


    // attach the Direct3D buffers to an FBO
    //glBindFramebuffer(GL_FRAMEBUFFER, 0);
    //glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, gl_names[0], 0);
    //glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D_MULTISAMPLE, gl_names[1]);
    //glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D_MULTISAMPLE, gl_names[1]);
    
    */

    MSG msg;
    while (true)
    {
        if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            if (msg.message == WM_QUIT)
                break;
        }
        else
        {
            // Render
            static float clearColor[4] { 1, 0, 0, 1 };
            

            // lock the render targets for GL access
            wglDXLockObjectsNV(gl_handleD3D, 1, &gl_handle);

            //glBindTexture(GL_TEXTURE_2D, gl_names[0]);
            //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
            //glBindFramebuffer(GL_FRAMEBUFFER, fboID);
            glClearColor(.1f, .1f, .1f, 1.f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            
            glBindTexture(GL_TEXTURE_2D, gl_tex);
            glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);
            //glFlush();
            //glFinish();

            shader.use();
            static int shader_tex0 = shader.getUniformLocation("tex0");
            glUniform1i(shader_tex0, TEX_DIFFUSE);
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            glClearColor(.1, .1, .1, 1);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//             glColor3f(1, 0, 1);
//             glBegin(GL_TRIANGLES);
//             glVertex3f(0, 0, 0);
//             glTexCoord2f(0, 0);
//             glVertex3f(1, 0, 0);
//             glTexCoord2f(1, 0);
//             glVertex3f(1, 1, 0);
//             glTexCoord2f(1, 1);
//             glEnd();
            plane.material.diffuse_tex = texID;
            plane.draw(GL_TRIANGLES);

            glfwSwapBuffers(window);

            // unlock the render targets
            wglDXUnlockObjectsNV(gl_handleD3D, 1, &gl_handle);

            ovrHmd_BeginFrameTiming(hmd, 0);
            context->ClearRenderTargetView(backbuffer, clearColor);
            swapchain->Present(0, 0);
            ovrHmd_EndFrameTiming(hmd);
        }
    }
    return 0;
}

